## install instructions for linux server
URL: https://www.spigotmc.org/wiki/linux-installation

```
mkdir Spigot_Server && cd Spigot_Server
sudo apt update
sudo apt-get install git openjdk-17-jre-headless -y

wget -O BuildTools.jar https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar

git config --global --unset core.autocrlf
java -jar BuildTools.jar --rev latest

ln -s spigot-1.18.2.jar spigot.jar

cat << EOF > start.sh
#!/bin/bash

#screen -S "minecraft" -U -m -d java -server -Xmx2G -jar spigot.jar
java -server -Xmx2G -jar spigot.jar
EOF

chmod +x start.sh

vi eula.txt
```
### geyser and floodgate
```

wget -O plugins/Geyser-Spigot.jar https://ci.opencollab.dev/job/GeyserMC/job/Geyser/job/master/lastSuccessfulBuild/artifact/bootstrap/spigot/target/Geyser-Spigot.jar

wget -O plugins/floodgate-spigot.jar https://ci.opencollab.dev/job/GeyserMC/job/Floodgate/job/master/lastSuccessfulBuild/artifact/spigot/target/floodgate-spigot.jar

```
### ports
19132

## install on windows pc
https://help.minecraft.net/hc/en-us/articles/4412266114573-Minecraft-on-Xbox-Game-Pass-#:~:text=Xbox%20Game%20Pass%20for%20PC,Minecraft%20for%20Windows%20on%20PC.

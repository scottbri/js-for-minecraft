var maniacs = ["Yakko", "Wakko", "Dot"];

console.log(maniacs);
console.log(maniacs[0] + " " + maniacs[1] + " " + maniacs[2]);

console.log("Third maniac: " + maniacs[3]);

for (i=0; i<maniacs.length; i++) {
    console.log(i + " is " + maniacs[i]);
}

console.log("The last maniac in the list is " + maniacs[maniacs.length - 1])

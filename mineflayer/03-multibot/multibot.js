// This is an example that uses mineflayer-pathfinder to showcase how simple it is to walk to goals

const mineflayer = require('mineflayer')
const pathfinder = require('mineflayer-pathfinder').pathfinder
const Movements = require('mineflayer-pathfinder').Movements
const { GoalNear } = require('mineflayer-pathfinder').goals

const botName = 'botscott' //TODO: customize botName
const bot = mineflayer.createBot({
  host: '192.168.1.105', // minecraft server ip
  username: botName // minecraft username
  //password: 'xxx', // minecraft password, comment out if you want to log into online-mode=false servers
  // port: 25565,                // only set if you need a port that isn't 25565
  // version: false,             // only set if you need a specific version or snapshot (ie: "1.8.9" or "1.16.5"), otherwise it's set automatically
  //auth: 'microsoft'              // only set if you need microsoft auth, then set this to 'microsoft'
})

bot.loadPlugin(pathfinder)

//bot.on('chat', (username, message) => {
//  if (message.startsWith("::")) return
//    bot.whisper(username, ":: " + message)
//})

// Log errors and kick reasons:
bot.on('kicked', console.log)
bot.on('error', console.log)

const { mineflayer: mineflayerViewer } = require('prismarine-viewer')
bot.once('spawn', () => {
  mineflayerViewer(bot, { port: 3007, firstPerson: false }) // port is the minecraft server port, if first person is false, you get a bird's-eye view
})

bot.once('spawn', () => {
  const mcData = require('minecraft-data')(bot.version)
  const defaultMove = new Movements(bot, mcData)
  
  bot.on('chat', function(username, message) {
    if (message.startsWith("::")) return // another bot chatter.  do nothing
   
    if (message == botName + ' ' + 'come') {
      const target = bot.players[username] ? bot.players[username].entity : null
      myBotGo(bot, username, target)
    }

    if (message == botName + ' ' + 'hello') {
        myBotSay(bot, username, 'Hello ' + username)
    }
    
    if (message == botName + ' ' + 'where') {
        myBotSay(bot, username, 'I am here: ' + "somewhere")
    }

  }) //bot.on chat
}) //bot.once spawn

//////// 
function myBotSay(bot, username, message) {
  bot.whisper(username, ':: ' + message)
}

//////// 
// TODO:  change to use myBotSay function
function myBotGo(bot, username, target) {
  if (!target) {
    bot.whisper(username, ':: I don\'t see you !')
    return //can't find target.  do nothing more
  } 

  const p = target.position

  bot.pathfinder.setMovements(defaultMove)
  bot.pathfinder.setGoal(new GoalNear(p.x, p.y, p.z, 2))
  myBotSay(bot, username, 'Coming...')
}

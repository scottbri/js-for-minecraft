// This is an example that uses mineflayer-pathfinder to showcase how simple it is to walk to goals

const mineflayer = require('mineflayer')
const pathfinder = require('mineflayer-pathfinder').pathfinder
const Movements = require('mineflayer-pathfinder').Movements
const { GoalNear } = require('mineflayer-pathfinder').goals
const collectBlock = require('mineflayer-collectblock').plugin

const botName = 'botscott' //TODO: customize botName
const bot = mineflayer.createBot({
  host: 'lordofservers.bekind.io', // minecraft server ip "localhost"
  username: botName // minecraft username
  //password: 'xxx', // minecraft password, comment out if you want to log into online-mode=false servers
  // port: 25565,                // only set if you need a port that isn't 25565
  // version: false,             // only set if you need a specific version or snapshot (ie: "1.8.9" or "1.16.5"), otherwise it's set automatically
  //auth: 'microsoft'              // only set if you need microsoft auth, then set this to 'microsoft'
})

bot.loadPlugin(pathfinder)
bot.loadPlugin(collectBlock)

//bot.on('chat', (username, message) => {
//  if (message.startsWith("::")) return
//    bot.whisper(username, ":: " + message)
//})

// Log errors and kick reasons:
bot.on('kicked', console.log)
bot.on('error', console.log)

const { mineflayer: mineflayerViewer } = require('prismarine-viewer')
bot.once('spawn', () => {
  mineflayerViewer(bot, { port: 3007, firstPerson: false }) // port is the minecraft server port, if first person is false, you get a bird's-eye view
})

bot.once('spawn', () => {
  const botData = require('minecraft-data')(bot.version)
  const defaultMove = new Movements(bot, botData)
  
  bot.on('chat', function(username, message) {
    if (message.startsWith("::")) return // another bot chatter.  do nothing
   
    if (message == botName + ' ' + 'come') {
      const target = bot.players[username] ? bot.players[username].entity : null
      myBotGo(bot, defaultMove, username, target)
    }

    if (message == botName + ' ' + 'hello') {
        myBotSay(bot, username, 'Hello ' + username)
    }
    
    if (message == botName + ' ' + 'where') {
        myBotSay(bot, username, 'I am here: ' + bot.entity.position)
    }
    
    if (message == botName + ' ' + 'drop') {
        myBotDrop(bot, username, 'Here you go... ')
    }
    
    if (message.startsWith(botName + ' ' + 'collect')) {
        const material = message.split(botName + ' ' + 'collect' + ' ')[1]  
        myBotSay(bot, username, 'Collecting ' + material)
        myBotCollect(botData, bot, username, material)
    }

  }) //bot.on chat
}) //bot.once spawn

//////// 
function myBotSay(bot, username, message) {
  bot.whisper(username, ':: ' + message)
  //bot.whisper(username, "hello")
}

//////// 
// TODO:  change to use myBotSay function
function myBotGo(bot, defaultMove, username, target) {
  if (!target) {
    myBotSay(bot, username, 'I don\'t see you !')
    return //can't find target.  do nothing more
  } 

  const p = target.position

  bot.pathfinder.setMovements(defaultMove)
  bot.pathfinder.setGoal(new GoalNear(p.x, p.y, p.z, 2))
  myBotSay(bot, username, 'Coming...')
}

////////
function myBotCollect (botData, bot, username, requestType) {
    // Get the correct block type
    const blockType = botData.blocksByName[requestType]
    const count = 1
    if (!blockType) {
      myBotSay(bot, username, "I don't know any blocks with that name.")
      myBotSay(bot, username, "Check Console Log for block types.")
      console.log(typeof(botData.blocksByName))
      for (const [key, value] of Object.entries(botData.blocksByName)) {
        console.log(`${key}`);
      }
      //console.log("blocks: ", botData.blocksByName )
      return
    }

    myBotSay(bot, username, 'Collecting the nearest ' + blockType.name)
  
    // Try and find that block type in the world
    const blocks = bot.findBlocks({
      matching: blockType.id,
      maxDistance: 64,
      count: count
    })
  
    if (blocks.length === 0) {
      myBotSay(bot, username, "I don't see that block nearby.")
      return
    }

    const targets = []
  for (let i = 0; i < Math.min(blocks.length, count); i++) {
    targets.push(bot.blockAt(blocks[i]))
    console.log(bot.blockAt(blocks[i]))
  }

  myBotSay(bot, username, `Found ${targets.length}`)

  try {
    bot.collectBlock.collect(targets)
    // All blocks have been collected.
    myBotSay(bot, username, 'Done')
  } catch (err) {
    // An error occurred, report it.
    myBotSay(bot, username, err.message)
    console.log(err)
  }
}

//////// 
function myBotDrop(bot) {
  const items = bot.inventory.items() // get the items
  console.log(items) // output inventory
const timeout = 1000 // timeout in milliseconds before dropping another item. 1 second = 1000ms

// dropper is a recursive function
const dropper = (i) => {
  if (!items[i]) return // if we dropped all items, stop.
  bot.tossStack(items[i], () => {
    setTimeout(() => dropper(i + 1), timeout) // wait for `timeout` ms, then drop another item
  })
}
dropper(0)
}
var secondsInAMinute = 60;
var minutesInAnHour = 60;
var secondsInAnHour = secondsInAMinute * minutesInAnHour;

console.log("Seconds: " + secondsInAnHour);

var numberOfSiblings = 1 + 3;
var numberOfCandies = 8;
console.log("Candies per Sibling: " + (numberOfCandies / numberOfSiblings));

var highfives = 0;
console.log(highfives++);
console.log(highfives);

var balloons = 100;
balloons *= 2;
console.log("balloons: " + balloons);
console.log("balloons: " + (balloons /= 4));
console.log("balloons: " + balloons);
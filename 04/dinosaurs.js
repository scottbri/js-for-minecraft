var dinosaurs = [
                { name: "Tyrannosaurus Rex", period: "Late Cretaceous" },
                { name: "Stegosaurus", period: "Late Jurassic" },
                { name: "Plateosaurus", period: "Triassic" }
];

console.log(dinosaurs[0]["name"]);

for (i=0; i<dinosaurs.length; i++) {
    console.log("");
    console.log("dinosaurs[" + i + "].name" + " is " + dinosaurs[i].name);
    console.log("It's from the " + dinosaurs[i].period + " period");
}

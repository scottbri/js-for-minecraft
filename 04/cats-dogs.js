var dog = { name: "Pancake", age: 6, color: "white", bark: "Yip yap yip!" };
var cat = { name: "Harmony", age: 8, color: "tortoiseshell" }; 

console.log(Object.keys(dog));

console.log(Object.keys(cat)); 
console.log(cat.name + " is a " + cat.color + " and is " + cat.age + " years old.");

cat.color = "tabby";
console.log(cat.name + " is a " + cat.color + " and is " + cat.age + " years old.");

cat.age = "10";
console.log(cat.name + " is a " + cat.color + " and is " + cat.age + " years old.");

